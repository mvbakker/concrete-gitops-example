#!/usr/bin/env bash

minikube stop
minikube delete
minikube start
kustomize build cluster/overlays/minikube | kubectl apply -f -
kustomize build cluster/overlays/minikube | kubectl apply -f -
